# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

class GuessingGame

  attr_reader :answer, :guess, :count

  def initialize
    @answer = 1 + rand(100)
    @guess = nil
    @count = 0
  end

  def guessing_game
    until won?
      @guess = guess?
      puts "Too low" if @guess < @answer
      puts "Too high" if @guess > @answer
    end
    congrats
  end

  def congrats
    puts "Congrats! You got it! :)"
    puts "You guessed the answer in #{count} attempt(s)"
  end

  def won?
    return true if @guess == @answer
    false
  end

  def valid_move?(guess)
    return false unless (1..100).include?(guess)
    true
  end

  def guess?
    print "Choose a number between (1-100): "
    @guess = gets.chomp.to_i
    until valid_move?(@guess)
      print "Input a valid Guess (1-100): "
      @guess = gets.chomp.to_i
    end
    @count += 1
    @guess
  end

  if __FILE__ == $PROGRAM_NAME
    g = GuessingGame.new
    g.guessing_game
  end

end
